class GridState:

    def __init__(self, x, y, number, allowChange):
        self.x = x
        self.y = y
        self.number = number
        self.allowChange = allowChange

    def canBeModified(self):
        return self.allowChange

    def setNewNumber(self, newNumber):
        if newNumber >= 0 and newNumber <= 9:
            self.number = newNumber

    def getNumberAsString(self):
        return str(self.number)