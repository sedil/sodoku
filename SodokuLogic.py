from random import shuffle

class SudokuLogic:

    def __init__(self, grid=None):
        self.counter = 0
        if(grid):
            """ Solve a given Sodoku """
            if len(grid[0]) == 9 and len(grid) == 9:
                self.grid = grid
                self.solveGivenSodoku()
            else:
                print("Input is Invalid! It must be a 9x9 matrix")
        else:
            """ Generate a new Sodoku """
            self.grid = [[0 for i in range(9)] for j in range(9)]
            self.generateSodoku()

    """ Solves a Sodoku Puzzle """
    def solveGivenSodoku(self):
        self.generateSolution()

    """ Generating a Sodoku puzzle """
    def generateSodoku(self):
        self.generateSolution()
        self.removeNumbersFromGrid()

    """ tests each square to make sure it is a valid puzzle """
    def examineSodoku(self):
        for row in range(9):
            for col in range(9):
                num = self.grid[row][col]

                # remove number from grid to test if it's valid
                self.grid[row][col] = 0
                if not self.validLocation(row, col, num):
                    return False
                else:
                    # put number back in grid
                    self.grid[row][col] = num
        return True

    """ returns True if the number has been used in that row """
    def numberUsedInRow(self, row, number):
        if number in self.grid[row]:
            return True
        return False

    """ returns True if the number has been used in that column """
    def numberUsedInColumn(self, col, number):
        for i in range(9):
            if self.grid[i][col] == number:
                return True
        return False

    """ returns True if the number has been used in that subgrid/box """
    def numberUsedInSubgrid(self, row, col, number):
        sub_row = (row // 3) * 3
        sub_col = (col // 3) * 3
        for i in range(sub_row, (sub_row + 3)):
            for j in range(sub_col, (sub_col + 3)):
                if self.grid[i][j] == number:
                    return True
        return False

    """ return False if the number has been used in the row, column or subgrid """
    def validLocation(self, row, col, number):
        if self.numberUsedInRow(row, number):
            return False
        elif self.numberUsedInColumn(col, number):
            return False
        elif self.numberUsedInSubgrid(row, col, number):
            return False
        return True

    """ return the next empty square coordinates in the grid """
    def findEmptySquare(self):
        for i in range(9):
            for j in range(9):
                if self.grid[i][j] == 0:
                    return (i, j)

    """ solve the sudoku puzzle with backtracking """
    def solveSodoku(self):
        for i in range(0, 81):
            row = i // 9
            col = i % 9

            # find next empty cell
            if self.grid[row][col] == 0:
                for number in range(1, 10):

                    # check that the number hasn't been used in the row/col/subgrid
                    if self.validLocation(row, col, number):
                        self.grid[row][col] = number
                        if not self.findEmptySquare():
                            self.counter += 1
                            break
                        else:
                            if self.solveSodoku():
                                return True
                break
        self.grid[row][col] = 0
        return False

    """ generates a full solution with backtracking """
    def generateSolution(self):
        number_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        for i in range(0, 81):
            row = i // 9
            col = i % 9

            # find next empty cell
            if self.grid[row][col] == 0:
                shuffle(number_list)
                for number in number_list:
                    if self.validLocation(row, col, number):
                        self.grid[row][col] = number
                        if not self.findEmptySquare():
                            return True
                        else:
                            if self.generateSolution():
                                # if the grid is full
                                return True
                break
        self.grid[row][col] = 0
        return False

    """ returns a shuffled list of non-empty squares in the puzzle """
    def getNonEmptySquare(self):
        non_empty_squares = []
        for i in range(len(self.grid)):
            for j in range(len(self.grid)):
                if self.grid[i][j] != 0:
                    non_empty_squares.append((i, j))
        shuffle(non_empty_squares)
        return non_empty_squares

    """ remove numbers from the grid to create the puzzle to solve manual """
    def removeNumbersFromGrid(self):

        # get all non-empty squares from the grid
        non_empty_squares = self.getNonEmptySquare()
        non_empty_squares_count = len(non_empty_squares)
        rounds = 3
        while rounds > 0 and non_empty_squares_count >= 17:

            # there should be at least 17 clues
            row, col = non_empty_squares.pop()
            non_empty_squares_count -= 1

            # might need to put the square value back if there is more than one solution
            removed_square = self.grid[row][col]
            self.grid[row][col] = 0

            # initialize solutions counter to zero
            self.counter = 0
            self.solveSodoku()

            # if there is more than one solution, put the last removed cell back into the grid
            if self.counter != 1:
                self.grid[row][col] = removed_square
                non_empty_squares_count += 1
                rounds -= 1

    def printGridField(self, grid_name=None):
        if grid_name:
            print(grid_name)
        for row in self.grid:
            print(row)

    def getSodokuPuzzle(self):
        return self.grid