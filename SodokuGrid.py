from SodokuLogic import SudokuLogic
from PyQt5.QtWidgets import QWidget, QVBoxLayout
from PyQt5.QtGui import QPainter, QColor, QFont

from GridState import GridState
from NumberDialog import NumberDialog

"""
The main UI for the Sodoku Game. It represents the Gridfield
"""
class SodokuGrid(QWidget):

    def __init__(self, parent=None):
        super(SodokuGrid, self).__init__(parent)
        self.GRIDSIZE = 60
        self.showProgress = False

        """
        generate an unsolved sodoku puzzle
        """
        self.puzzle = SudokuLogic()

        """
        create for every grid some information like: clicked coordinate, number
        and if it can be modified or not
        """
        self.ui_grid = [[None] * 9 for _ in range(9)]
        for i in range(9):
            for j in range(9):
                if self.puzzle.getSodokuPuzzle()[j][i] == 0:
                     self.ui_grid[j][i] = GridState(j, i, 0, True)
                else:
                    self.ui_grid[j][i] = GridState(j, i, self.puzzle.getSodokuPuzzle()[j][i], False)
        self.solution = SudokuLogic(self.puzzle.getSodokuPuzzle())

        self.setMouseTracking(True)
        self.initColors()
        self.initComponents()
        self.setLayout(self._layout)
        self.setGeometry(100, 100, 9*self.GRIDSIZE, 9*self.GRIDSIZE)
        self.setFixedSize(9*self.GRIDSIZE, 9*self.GRIDSIZE)
        self.show()

    def initComponents(self):
        self._layout = QVBoxLayout()

    def newSodokuGame(self):
        self.puzzle = SudokuLogic()

        """
        create for every grid some information like: clicked coordinate, number
        and if it can be modified or not
        """
        self.ui_grid = [[None] * 9 for _ in range(9)]
        for i in range(9):
            for j in range(9):
                if self.puzzle.getSodokuPuzzle()[j][i] == 0:
                     self.ui_grid[j][i] = GridState(j, i, 0, True)
                else:
                    self.ui_grid[j][i] = GridState(j, i, self.puzzle.getSodokuPuzzle()[j][i], False)
        self.solution = SudokuLogic(self.puzzle.getSodokuPuzzle())
        self.update()

    def initColors(self):
        self.COLOR_LIGHTGREY = QColor(190, 190, 190)
        self.COLOR_WHITE = QColor(255, 255, 255)
        self.COLOR_BLACK = QColor(0, 0, 0)
        self.COLOR_RED = QColor(255, 0, 0)
        self.COLOR_GREEN = QColor(34, 139, 34)

    """
    calculate a solution for the given sodoku and show it
    """
    def showSolution(self):
        #self.solution = SudokuLogic(self.puzzle.getSodokuPuzzle())
        for i in range(9):
            for j in range(9):
                self.ui_grid[j][i].setNewNumber(self.solution.getSodokuPuzzle()[j][i])
        self.update()

    def showProgressOnSodoku(self):
        if self.showProgress:
            self.showProgress = False
        else:
            self.showProgress = True
        self.update()

    def mousePressEvent(self, mouseevent):
        x = int(mouseevent.x() / self.GRIDSIZE)
        y = int(mouseevent.y() / self.GRIDSIZE)
        NumberDialog(self,x,y)

    def tryToSetNumberInGrid(self, x, y, number):
        if self.ui_grid[y][x].canBeModified():
            if number == "c":
                self.ui_grid[y][x].setNewNumber(0)
            else:
                self.ui_grid[y][x].setNewNumber(int(number))
        self.update()

    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)
        self.clear(painter)
        self.drawRectanglePattern(painter)
        self.drawNumbers(painter)
        painter.end()

    def clear(self, painter):
        painter.setPen(self.COLOR_LIGHTGREY)
        painter.setBrush(self.COLOR_LIGHTGREY)
        painter.drawRect(0, 0, 9*self.GRIDSIZE, 9*self.GRIDSIZE)

    def drawRectanglePattern(self, painter):
        painter.setPen(self.COLOR_LIGHTGREY)
        painter.setBrush(self.COLOR_LIGHTGREY)
        painter.drawRect(0, 0, 9*self.GRIDSIZE, 9*self.GRIDSIZE)

        # 3x3 Muster
        painter.setBrush(self.COLOR_WHITE)
        painter.drawRect(3 * self.GRIDSIZE, 0, 3 * self.GRIDSIZE, 3 * self.GRIDSIZE)
        painter.drawRect(0, 3 * self.GRIDSIZE, 3 * self.GRIDSIZE, 3 * self.GRIDSIZE)
        painter.drawRect(6 * self.GRIDSIZE, 3 * self.GRIDSIZE, 3 * self.GRIDSIZE, 3 * self.GRIDSIZE)
        painter.drawRect(3 * self.GRIDSIZE, 6 * self.GRIDSIZE, 3 * self.GRIDSIZE, 3 * self.GRIDSIZE)

        # Lines
        painter.setPen(self.COLOR_BLACK)
        for i in range(9):
            painter.drawLine(0, i * self.GRIDSIZE, 9 * self.GRIDSIZE, i * self.GRIDSIZE)
        for j in range(9):
            painter.drawLine(j * self.GRIDSIZE, 0, j * self.GRIDSIZE, 9 * self.GRIDSIZE)

    def drawNumbers(self, painter):
        font = QFont()
        font.setBold(True)
        font.setPointSize(26)
        painter.setFont(font)

        for i in range(9):
            for j in range(9):
                # empty grids
                if(self.ui_grid[j][i].getNumberAsString() == "0"):
                    painter.drawText(i * self.GRIDSIZE + int(self.GRIDSIZE / 3), (j + 1) * self.GRIDSIZE - int(self.GRIDSIZE / 3), " ")
                # manual set numbers in grid
                elif(self.ui_grid[j][i].canBeModified() and self.ui_grid[j][i].getNumberAsString() != "0" and not self.showProgress):
                    painter.setPen(self.COLOR_RED)
                    painter.drawText(i*self.GRIDSIZE + int(self.GRIDSIZE/3), (j+1)*self.GRIDSIZE - int(self.GRIDSIZE/3), self.ui_grid[j][i].getNumberAsString())
                # examine correct numbers for each field
                elif(self.ui_grid[j][i].canBeModified() and self.ui_grid[j][i].getNumberAsString() == str(self.solution.getSodokuPuzzle()[j][i]) and self.showProgress):
                    painter.setPen(self.COLOR_GREEN)
                    painter.drawText(i*self.GRIDSIZE + int(self.GRIDSIZE/3), (j+1)*self.GRIDSIZE - int(self.GRIDSIZE/3), self.ui_grid[j][i].getNumberAsString())
                # draw all rest numbers within examine mode
                elif(not self.ui_grid[j][i].canBeModified() and self.ui_grid[j][i].getNumberAsString() == str(self.solution.getSodokuPuzzle()[j][i]) and self.showProgress):
                    painter.setPen(self.COLOR_BLACK)
                    painter.drawText(i*self.GRIDSIZE + int(self.GRIDSIZE/3), (j+1)*self.GRIDSIZE - int(self.GRIDSIZE/3), self.ui_grid[j][i].getNumberAsString())
                # draw all rest numbers without examine mode
                elif(not self.ui_grid[j][i].canBeModified() and self.ui_grid[j][i].getNumberAsString() != "0" and not self.showProgress):
                    painter.setPen(self.COLOR_BLACK)
                    painter.drawText(i*self.GRIDSIZE + int(self.GRIDSIZE/3), (j+1)*self.GRIDSIZE - int(self.GRIDSIZE/3), self.ui_grid[j][i].getNumberAsString())