from PyQt5.QtWidgets import QWidget, QPushButton, QGridLayout
from PyQt5.QtCore import Qt

"""
A helper class to choose a number for a given Grid
"""
class NumberDialog(QWidget):

    def __init__(self, sodoku_ui, x, y, parent=None):
        super(NumberDialog, self).__init__(parent)
        self.sodoku_ui = sodoku_ui

        """
        The Grid coordinates which should be filled with a number
        """
        self.coordX = x
        self.coordY = y

        self.setWindowTitle("Choose a number")
        self.setWindowModality(Qt.ApplicationModal)
        self.initComponents()
        self.setLayout(self.layout)
        self.show()

    def initComponents(self):
        self.layout = QGridLayout()
        self.btnArray = 10 * [None]
        self.btnArray[0] = QPushButton(str(1))
        self.btnArray[0].clicked.connect(lambda : self.numberChooser(self.btnArray[0]))
        self.btnArray[1] = QPushButton(str(2))
        self.btnArray[1].clicked.connect(lambda : self.numberChooser(self.btnArray[1]))
        self.btnArray[2] = QPushButton(str(3))
        self.btnArray[2].clicked.connect(lambda : self.numberChooser(self.btnArray[2]))
        self.btnArray[3] = QPushButton(str(4))
        self.btnArray[3].clicked.connect(lambda : self.numberChooser(self.btnArray[3]))
        self.btnArray[4] = QPushButton(str(5))
        self.btnArray[4].clicked.connect(lambda : self.numberChooser(self.btnArray[4]))
        self.btnArray[5] = QPushButton(str(6))
        self.btnArray[5].clicked.connect(lambda : self.numberChooser(self.btnArray[5]))
        self.btnArray[6] = QPushButton(str(7))
        self.btnArray[6].clicked.connect(lambda : self.numberChooser(self.btnArray[6]))
        self.btnArray[7] = QPushButton(str(8))
        self.btnArray[7].clicked.connect(lambda : self.numberChooser(self.btnArray[7]))
        self.btnArray[8] = QPushButton(str(9))
        self.btnArray[8].clicked.connect(lambda : self.numberChooser(self.btnArray[8]))
        self.btnArray[9] = QPushButton("c")
        self.btnArray[9].clicked.connect(lambda : self.numberChooser(self.btnArray[9]))

        for i in range(3):
            for j in range(3):
                self.layout.addWidget(self.btnArray[3*i+j],i,j)
        self.layout.addWidget(self.btnArray[9],4,1)

    def numberChooser(self, button):
        self.sodoku_ui.tryToSetNumberInGrid(self.coordX, self.coordY, button.text())
        self.close()