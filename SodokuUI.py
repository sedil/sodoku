import sys
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QHBoxLayout, QMenu, QMenuBar, QAction
from SodokuGrid import SodokuGrid

"""
This class starts the application
"""
class SodokuUI(QWidget):

    def __init__(self, parent=None):
        super(SodokuUI, self).__init__(parent)
        self.grid = SodokuGrid()
        self.initComponents()
        self.setLayout(self._layout)
        self.setWindowTitle("Sodoku")
        self.show()

    def initComponents(self):
        self._layout = QVBoxLayout()

        self.menubar = QMenuBar()
        self.menu = QMenu("Game")
        self.menu_newGame = QAction("New Sodoku")
        self.menu_newGame.setShortcut("Ctrl+N")
        self.menu_newGame.triggered.connect(self.menuChooser)
        self.menu_showSolution = QAction("Show Solution")
        self.menu_showSolution.setShortcut("Ctrl+S")
        self.menu_showSolution.triggered.connect(self.menuChooser)
        self.menu_examineProgress = QAction("Examine Progress")
        self.menu_examineProgress.setShortcut("Ctrl+P")
        self.menu_examineProgress.triggered.connect(self.menuChooser)
        self.menu_quitGame = QAction("Quit")
        self.menu_quitGame.setShortcut("Ctrl+Q")
        self.menu_quitGame.triggered.connect(self.menuChooser)

        self.menu.addAction(self.menu_newGame)
        self.menu.addAction(self.menu_showSolution)
        self.menu.addAction(self.menu_examineProgress)
        self.menu.addAction(self.menu_quitGame)
        self.menubar.addMenu(self.menu)

        self._layout.addWidget(self.menubar)
        self._layout.addWidget(self.grid)

    def menuChooser(self):
        action = self.sender().text()
        if action == "New Sodoku":
            self.grid.newSodokuGame()
        elif action == "Show Solution":
            self.grid.showSolution()
        elif action == "Examine Progress":
            self.grid.showProgressOnSodoku()
        elif action == "Quit":
            self.close()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    gui = SodokuUI()
    sys.exit(app.exec_())